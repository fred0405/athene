import express from 'express';
import {ApiControllers} from '../controllers/controllers';

const Controllers = new ApiControllers;

export class ApiRouter{
	router: express.Router;
	constructor(){
		this.router = express.Router();
		this.initializeRoutes();
	}
	initializeRoutes(){
		this.router.get('/', Controllers.getHomePage);
	}
}
