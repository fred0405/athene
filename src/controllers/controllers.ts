import express from 'express';

export class ApiControllers{
	getHomePage(req: express.Request, res:express.Response, next: express.NextFunction){
		res.type('text/plain');
		res.send("HomePage");
	}
}
