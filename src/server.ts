//app.ts
import express, { Express, Request, Response, NextFunction } from 'express';
import {ApiRouter} from './routes/routes';
const app = express();
const port = 405;
const apiRouter = new ApiRouter;


app.use((request: Request, response: Response, next: NextFunction) => {
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setHeader(
		"Access-Control-Allow-Headers",
		"Origin, X-Requested-With, Content-Type, Accept, Authorization"
	);
	response.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE");
	
	next();
});

app.use('', apiRouter.router)

app.use((request: Request, response: Response) => {
	response.type('text/plain');
	response.status(404)
	response.send('Page is not found.');
})

app.use(function(req, res, next){
	console.log(req.method, req.url);
});


app.listen(port,() => {
	console.log("The Athene is working on http://localhost:405");
});
